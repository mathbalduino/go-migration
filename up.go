package loxe

import (
	"database/sql"
	"fmt"
	"math"
)

func (m *migrator) UpTo(toVersion uint64) error {
	m.targetVersion = toVersion
	isDesiredVersion := func(actualVersion, migrationVersion uint64) bool {
		return actualVersion < migrationVersion && migrationVersion <= toVersion
	}
	return m.up(isDesiredVersion)
}

func (m *migrator) Up() error {
	m.targetVersion = math.MaxUint64
	isDesiredVersion := func(actualVersion, migrationVersion uint64) bool {
		return actualVersion < migrationVersion
	}
	return m.up(isDesiredVersion)
}

func (m *migrator) up(isDesiredVersion func(actualVersion, migrationVersion uint64) bool) error {
	var e error

	switch m.config.TxStyle {
	case TxPerOperation:
		e = m.upTxPerMigration(isDesiredVersion)
	case UniqueTx:
		e = m.upUniqueTx(isDesiredVersion)
	}

	if m.targetVersion <= m.initialVersion {
		fmt.Printf("The target version (v%d) is lower/equal than the actual version (v%d)\n", m.targetVersion, m.initialVersion)
		return e
	}
	if m.finalVersion > m.initialVersion {
		fmt.Printf("Database upgraded from v%d to v%d\n", m.initialVersion, m.finalVersion)
		return e
	}

	fmt.Printf("There are no migrations greater than the actual version (v%d)\n", m.initialVersion)
	return e
}

func (m *migrator) upUniqueTx(isDesiredVersion func(actualVersion, migrationVersion uint64) bool) error {
	return m.transaction(func(tx *sql.Tx) error {
		e := m.prepareDB(tx)
		if e != nil {
			return e
		}

		m.initialVersion, e = m.schemaVersion(tx)
		if e != nil {
			return e
		}

		if m.targetVersion <= m.initialVersion {
			return nil
		}

		m.finalVersion = m.initialVersion
		for _, migration := range m.migrations {
			migrationVersion := migration.Version()
			if !isDesiredVersion(m.initialVersion, migrationVersion) {
				continue
			}

			_, e := tx.Exec(migration.Up())
			if e != nil {
				return e
			}
			m.finalVersion = migrationVersion
		}

		if m.finalVersion > m.initialVersion {
			_, e = tx.Exec(fmt.Sprintf(updateSchemaVersionSQL, m.config.Schema, m.config.SchemaVersionTableName, m.finalVersion))
			if e != nil {
				return e
			}
		}

		return nil
	})
}

func (m *migrator) upTxPerMigration(isDesiredVersion func(actualVersion, migrationVersion uint64) bool) error {
	e := m.prepareDB(nil)
	if e != nil {
		return e
	}

	m.initialVersion, e = m.schemaVersion(nil)
	if e != nil {
		return e
	}

	if m.targetVersion <= m.initialVersion {
		return nil
	}

	m.finalVersion = m.initialVersion
	for _, migration := range m.migrations {
		migrationVersion := migration.Version()
		if !isDesiredVersion(m.initialVersion, migrationVersion) {
			continue
		}

		e = m.transaction(func(tx *sql.Tx) error {
			_, e := tx.Exec(migration.Up())
			if e != nil {
				return e
			}

			_, e = tx.Exec(fmt.Sprintf(updateSchemaVersionSQL, m.config.Schema, m.config.SchemaVersionTableName, migrationVersion))
			if e != nil {
				return e
			}

			return nil
		})
		if e != nil {
			return e
		}
		m.finalVersion = migrationVersion
	}

	return nil
}
